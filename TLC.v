module TLC (
input clk,
input reset,
output reg [2:0] north_l,
output reg [2:0] south_l,
output reg [2:0] east_l,
output reg [2:0] west_l
);

// define all states
localparam north   = 3'd0; 
localparam north_y = 3'd1; 
localparam south   = 3'd2; 
localparam south_y = 3'd3; 
localparam east    = 3'd4; 
localparam east_y  = 3'd5;
localparam west    = 3'd6;
localparam west_y  = 3'd7;

// define registers
reg [2:0] current_state;
reg [2:0] count;

// define constants 
localparam [2:0] red = 3'b100;
localparam [2:0] yellow = 3'b010;
localparam [2:0] green = 3'b001;

// states flow 
always @ (posedge clk, posedge reset) 
begin
     if (reset) 
     begin
	current_state = north;
	count  <= 3'b000; 
     end
     else
     begin
	case (current_state)
// north
	north:
	begin
		if (count == 3'b111) 
		begin
		    count = 3'b000;
		    current_state = north_y;
		end
		else
		begin
		    count = count + 3'b001;
		    current_state = north;
		end
	end
	north_y:
	begin
		if (count == 3'b011) 
		begin
		    count = 3'b000;
		    current_state = south;
		end
		else
		begin
		    count = count + 3'b001;
		    current_state = north_y;
		end
	end
// south
	south:
	begin
		if (count == 3'b111) 
		begin
		    count = 3'b000;
		    current_state = south_y;
		end
		else
		begin
		    count = count + 3'b001;
		    current_state = south;
		end
	end
	south_y:
	begin
		if (count == 3'b011) 
		begin
		    count = 3'b000;
		    current_state = east;
		end
		else
		begin
		    count = count + 3'b001;
		    current_state = south_y;
		end
	end
// east
	east:
	begin
		if (count == 3'b111) 
		begin
		    count = 3'b000;
		    current_state = east_y;
		end
		else
		begin
		    count = count + 3'b001;
		    current_state = east;
		end
	end
	east_y:
	begin
		if (count == 3'b011) 
		begin
		    count = 3'b000;
		    current_state = west;
		end
		else
		begin
		    count = count + 3'b001;
		    current_state = east_y;
		end
	end
// west 
	west:
	begin
		if (count == 3'b111) 
		begin
		    count = 3'b000;
		    current_state = west_y;
		end
		else
		begin
		    count = count + 3'b001;
		    current_state = west;
		end
	end
	west_y:
	begin
		if (count == 3'b011) 
		begin
		    count = 3'b000;
		    current_state = north;
		end
		else
		begin
		    count = count + 3'b001;
		    current_state = west_y;
		end
	end
	endcase
     end
end


// define states
always @ (current_state)
begin 
	north_l = red;
	south_l = red;
	east_l  = red;
	west_l  = red;
	case(current_state)
// north
	  north:
	  begin
	    north_l = green;
	  end
	  north_y:
	  begin
	    north_l = yellow;
	  end
// south
	  south:
	  begin
	    south_l = green;
	  end
	  south_y:
	  begin
	    south_l = yellow;
	  end
// east
	  east:
	  begin
	    east_l = green;
	  end
	  east_y:
	  begin
	    east_l = yellow;
	  end
// west
	  west:
	  begin
	    west_l = green;
	  end
	  west_y:
	  begin
	    west_l = yellow;
	  end
	endcase // case end
end // always end

endmodule 
