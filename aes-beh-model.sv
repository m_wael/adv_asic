typedef enum {encrypt, decrypt} aes_func;

// Key length	Nk		Nr
//		128		 4		10
//		192		 6		12
//		256		 8		14

class aes_beh_model #(int Nk=4, int Nr=10, aes_func func=decrypt);
	// Refer to section 5 fig.4 of FIPS-197 spec for definitions of Nk and Nr
	//				

	
	byte unsigned state[0:3][0:3];
	byte unsigned key_state[0:4*(Nr+1)-1][0:3];
	protected int unsigned i_round;
	bit done;	// done=1 -> decryption done, valid plaintext available for read.
	bit loaded;	// Ciphertext loaded, ready to start decryption.

	function new();	// Constructor
		done = 0;
		loaded = 0;
	endfunction

	function int unsigned GetCurrRound;
		// Returns which round we are at in the decryption process. For AES decryption
		// round counts down from Nr to 0.
		GetCurrRound = i_round;
	endfunction
	
	/////////////////////////////////////// start build states
	task LoadCt(bit [0:127] ct);
	// Populate state array with ciphertext and set loaded flag
		if (func == decrypt)
		begin
			for (int col=0; col<=3; col++) begin 
				{ >> { {state[0][col] , state[1][col] , state[2][col] , state[3][col]} } } = ct[(32*col)+:32];
			end
			loaded = 1;
			done = 0;
			i_round = Nr;	// Inverse cipher round counts down from Nr
		end
		else
			$display("#Info : aes_beh_model::LoadCt() cannot load ciphertext to encryptor.");
	endtask
	
	task LoadPt(bit [0:127] pt);
	// Populate state array with plaintext and set loaded flag
		if (func == encrypt)
		begin
    		for (int col=0; col<=3; col++) begin 
        		{ >> { {state[0][col] , state[1][col] , state[2][col] , state[3][col]} } } = pt[(32*col)+:32];
    		end
			loaded = 1;
			done = 0;
			i_round = 0;	// Cipher round counts up from 0
		end
		else
			$display("#Info : aes_beh_model::LoadPt() cannot load plaintext to decryptor.");
	endtask

	/////////////////////////////////////// enough build states
	
	function bit [0:127] state2array;
	// Returns current state as a 128-bit vector.
	// Once all rounds are completed, state contains the decrypted plaintext.
        for (int col=0; col<=3; col++)
            state2array[32*col+:32] = { >> {state[0][col] , state[1][col] , state[2][col] , state[3][col]}};
	endfunction

	function bit [0:127] printkey;
	// Get key schedule of the current round.
	// Note that for decryption, round counts down from Nr.
    	for (int row=0; row<=3; row++)
        	for (int col=0; col<=3; col++) printkey[(32*row+8*col)+:8] = key_state[i_round*4+row][col];
	endfunction

/////// Not needed
	function bit [0:127] LookupKsch(int unsigned r);
	// Lookup key schedule for any round.
		for (int j=0; j<=3; j++)
			for (int k=0; k<=3; k++) LookupKsch[(32*j+8*k)+:8] = key_state[r*4+j][k];
	endfunction
///// Not needed	

	/////////////////// End sbox 

	protected function byte unsigned inv_sbox(byte unsigned x);
	// Inverse Sbox table
	const byte unsigned inv_sbox_table[0:255] = {
	//        0     1     2     3     4     5     6     7     8     9     a     b     c     d     e     f
		8'h52,8'h09,8'h6a,8'hd5,8'h30,8'h36,8'ha5,8'h38,8'hbf,8'h40,8'ha3,8'h9e,8'h81,8'hf3,8'hd7,8'hfb, //0
		8'h7c,8'he3,8'h39,8'h82,8'h9b,8'h2f,8'hff,8'h87,8'h34,8'h8e,8'h43,8'h44,8'hc4,8'hde,8'he9,8'hcb, // 1
		8'h54,8'h7b,8'h94,8'h32,8'ha6,8'hc2,8'h23,8'h3d,8'hee,8'h4c,8'h95,8'h0b,8'h42,8'hfa,8'hc3,8'h4e, // 2
		8'h08,8'h2e,8'ha1,8'h66,8'h28,8'hd9,8'h24,8'hb2,8'h76,8'h5b,8'ha2,8'h49,8'h6d,8'h8b,8'hd1,8'h25, // 3
		8'h72,8'hf8,8'hf6,8'h64,8'h86,8'h68,8'h98,8'h16,8'hd4,8'ha4,8'h5c,8'hcc,8'h5d,8'h65,8'hb6,8'h92, // 4
		8'h6c,8'h70,8'h48,8'h50,8'hfd,8'hed,8'hb9,8'hda,8'h5e,8'h15,8'h46,8'h57,8'ha7,8'h8d,8'h9d,8'h84, // 5
		8'h90,8'hd8,8'hab,8'h00,8'h8c,8'hbc,8'hd3,8'h0a,8'hf7,8'he4,8'h58,8'h05,8'hb8,8'hb3,8'h45,8'h06, // 6
		8'hd0,8'h2c,8'h1e,8'h8f,8'hca,8'h3f,8'h0f,8'h02,8'hc1,8'haf,8'hbd,8'h03,8'h01,8'h13,8'h8a,8'h6b, // 7
		8'h3a,8'h91,8'h11,8'h41,8'h4f,8'h67,8'hdc,8'hea,8'h97,8'hf2,8'hcf,8'hce,8'hf0,8'hb4,8'he6,8'h73, // 8
		8'h96,8'hac,8'h74,8'h22,8'he7,8'had,8'h35,8'h85,8'he2,8'hf9,8'h37,8'he8,8'h1c,8'h75,8'hdf,8'h6e, // 9
		8'h47,8'hf1,8'h1a,8'h71,8'h1d,8'h29,8'hc5,8'h89,8'h6f,8'hb7,8'h62,8'h0e,8'haa,8'h18,8'hbe,8'h1b, // A
		8'hfc,8'h56,8'h3e,8'h4b,8'hc6,8'hd2,8'h79,8'h20,8'h9a,8'hdb,8'hc0,8'hfe,8'h78,8'hcd,8'h5a,8'hf4, // B
		8'h1f,8'hdd,8'ha8,8'h33,8'h88,8'h07,8'hc7,8'h31,8'hb1,8'h12,8'h10,8'h59,8'h27,8'h80,8'hec,8'h5f, // C
		8'h60,8'h51,8'h7f,8'ha9,8'h19,8'hb5,8'h4a,8'h0d,8'h2d,8'he5,8'h7a,8'h9f,8'h93,8'hc9,8'h9c,8'hef, // D
		8'ha0,8'he0,8'h3b,8'h4d,8'hae,8'h2a,8'hf5,8'hb0,8'hc8,8'heb,8'hbb,8'h3c,8'h83,8'h53,8'h99,8'h61, // E 
		8'h17,8'h2b,8'h04,8'h7e,8'hba,8'h77,8'hd6,8'h26,8'he1,8'h69,8'h14,8'h63,8'h55,8'h21,8'h0c,8'h7d	 // F 
	};
		inv_sbox = inv_sbox_table[x];
	endfunction

	protected function byte unsigned sbox(byte unsigned x);
	// sbox_table 
	const byte unsigned sbox_table[0:255] = {
	//     0     1     2     3     4     5     6     7     8     9     a     b     c     d     e     f
		8'h63,8'h7c,8'h77,8'h7b,8'hf2,8'h6b,8'h6f,8'hc5,8'h30,8'h01,8'h67,8'h2b,8'hfe,8'hd7,8'hab,8'h76, // 0
		8'hca,8'h82,8'hc9,8'h7d,8'hfa,8'h59,8'h47,8'hf0,8'had,8'hd4,8'ha2,8'haf,8'h9c,8'ha4,8'h72,8'hc0, // 1
		8'hb7,8'hfd,8'h93,8'h26,8'h36,8'h3f,8'hf7,8'hcc,8'h34,8'ha5,8'he5,8'hf1,8'h71,8'hd8,8'h31,8'h15, // 2
		8'h04,8'hc7,8'h23,8'hc3,8'h18,8'h96,8'h05,8'h9a,8'h07,8'h12,8'h80,8'he2,8'heb,8'h27,8'hb2,8'h75, // 3
		8'h09,8'h83,8'h2c,8'h1a,8'h1b,8'h6e,8'h5a,8'ha0,8'h52,8'h3b,8'hd6,8'hb3,8'h29,8'he3,8'h2f,8'h84, // 4
		8'h53,8'hd1,8'h00,8'hed,8'h20,8'hfc,8'hb1,8'h5b,8'h6a,8'hcb,8'hbe,8'h39,8'h4a,8'h4c,8'h58,8'hcf, // 5
		8'hd0,8'hef,8'haa,8'hfb,8'h43,8'h4d,8'h33,8'h85,8'h45,8'hf9,8'h02,8'h7f,8'h50,8'h3c,8'h9f,8'ha8, // 6
		8'h51,8'ha3,8'h40,8'h8f,8'h92,8'h9d,8'h38,8'hf5,8'hbc,8'hb6,8'hda,8'h21,8'h10,8'hff,8'hf3,8'hd2, // 7
		8'hcd,8'h0c,8'h13,8'hec,8'h5f,8'h97,8'h44,8'h17,8'hc4,8'ha7,8'h7e,8'h3d,8'h64,8'h5d,8'h19,8'h73, // 8
		8'h60,8'h81,8'h4f,8'hdc,8'h22,8'h2a,8'h90,8'h88,8'h46,8'hee,8'hb8,8'h14,8'hde,8'h5e,8'h0b,8'hdb, // 9
		8'he0,8'h32,8'h3a,8'h0a,8'h49,8'h06,8'h24,8'h5c,8'hc2,8'hd3,8'hac,8'h62,8'h91,8'h95,8'he4,8'h79, // A
		8'he7,8'hc8,8'h37,8'h6d,8'h8d,8'hd5,8'h4e,8'ha9,8'h6c,8'h56,8'hf4,8'hea,8'h65,8'h7a,8'hae,8'h08, // B
		8'hba,8'h78,8'h25,8'h2e,8'h1c,8'ha6,8'hb4,8'hc6,8'he8,8'hdd,8'h74,8'h1f,8'h4b,8'hbd,8'h8b,8'h8a, // C
		8'h70,8'h3e,8'hb5,8'h66,8'h48,8'h03,8'hf6,8'h0e,8'h61,8'h35,8'h57,8'hb9,8'h86,8'hc1,8'h1d,8'h9e, // D
		8'he1,8'hf8,8'h98,8'h11,8'h69,8'hd9,8'h8e,8'h94,8'h9b,8'h1e,8'h87,8'he9,8'hce,8'h55,8'h28,8'hdf, // E
		8'h8c,8'ha1,8'h89,8'h0d,8'hbf,8'he6,8'h42,8'h68,8'h41,8'h99,8'h2d,8'h0f,8'hb0,8'h54,8'hbb,8'h16  // F
	};
		sbox = sbox_table[x];
	endfunction

	/////////////////////////////////////////// End sbox
	
	task key_expansion(bit [0:4*8*Nk-1] key);

    	int id=0;
		byte unsigned temp[0:3];
		byte unsigned Rcon[1:11] = {8'h01,8'h02,8'h04,8'h08,8'h10,8'h20,8'h40,8'h80,8'h1b,8'h36,8'h6c};
		byte unsigned kt[0:4*Nk-1];	

		for (int i=0; i<=4*Nk-1; i++) kt[i] = key[i*8+:8];

		while (id < Nk)
		begin
			key_state[id][0] = kt[4*id];
			key_state[id][1] = kt[4*id+1];
			key_state[id][2] = kt[4*id+2];
			key_state[id][3] = kt[4*id+3];
			id++;
		end

		while (id < 4*(Nr+1))
		begin
			temp[0] = key_state[id-1][0];
			temp[1] = key_state[id-1][1];
			temp[2] = key_state[id-1][2];
			temp[3] = key_state[id-1][3];

			if ((id % Nk) == 0) 
			begin
				RotWord(temp);
				SubWord(temp);
				temp[0] ^= Rcon[id/Nk];
			end
			else if ((Nk > 6) && ((id % Nk) == 4)) // Only Nk=8 (AES256) 
				SubWord(temp);
			
			key_state[id][0] = key_state[id-Nk][0] ^ temp[0];
			key_state[id][1] = key_state[id-Nk][1] ^ temp[1];
			key_state[id][2] = key_state[id-Nk][2] ^ temp[2];
			key_state[id][3] = key_state[id-Nk][3] ^ temp[3];
			
			id++;
		end
			
	endtask

	protected task SubWord(inout byte unsigned x[0:3]);
		x[0] = sbox(x[0]);
		x[1] = sbox(x[1]);
		x[2] = sbox(x[2]);
		x[3] = sbox(x[3]);
	endtask

	protected task RotWord(inout byte unsigned x[0:3]);
		byte unsigned tmp;

		tmp = x[0];
		x[0] = x[1];
		x[1] = x[2];
		x[2] = x[3];
		x[3] = tmp;
	endtask

	////////////////////////////////////////////////////////////////////////////////////////////////////// End Key generation

	////////////////////////////////////////////////////////////////////////////////////////////////////// start multiplication 
	protected function byte unsigned xtime(byte unsigned x);
	// Multiplication by 2 over GF(256)
	// Refer to FIPS-197 spec section 4.2.1 on definition of GF(256) multiplication
		xtime = (x[7])? (x<<1) ^ 8'h1b : x<<1;
	endfunction

	protected function byte unsigned mul2(byte unsigned x);
	// Same as xtime(). For improved readibility only.
		mul2 = xtime(x);
	endfunction
	
	protected function byte unsigned mul3(byte unsigned x);
	// Multiply by 3 over GF(256)
	// 3*x = 2*x + x
	// Addition over GF(256) is xor
		mul3 = xtime(x) ^ x;
	endfunction
	
	protected function byte unsigned mul4(byte unsigned x);
	// Multiply by 4 over GF(256)
	// 4*x = 2*(2*x)
		mul4 = xtime(xtime(x));
	endfunction

	protected function byte unsigned mul8(byte unsigned x);
	// Multiply by 8 over GF(256)
	// 8*x = 2*(4*x)
		mul8 = xtime(mul4(x));
	endfunction

	protected function byte unsigned mul9(byte unsigned x);
	// Multiply by 9 over GF(256)
	// 9*x = 8*x + x
	// Addition over GF(256) is xor
		mul9 = mul8(x) ^ x;
	endfunction

	protected function byte unsigned mulb(byte unsigned x);
	// Multiply by 0xb over GF(256)
	// b*x = 8*x + 2*x +x
		mulb = mul8(x) ^ xtime(x) ^ x;
	endfunction

	protected function byte unsigned muld(byte unsigned x);
	// Multiply by 0xd over GF(256)
	// d*x = 8*x + 4*x + x
		muld = mul8(x) ^ mul4(x) ^ x;
	endfunction

	protected function byte unsigned mule(byte unsigned x);
	// Multiply by 0xe over GF(256)
	// e*x = 8*x + 4*x +2*x
		mule = mul8(x) ^ mul4(x) ^ xtime(x);
	endfunction

//////////////////////////////////////////////////////////////////////////////////////////////////////// Multiplication 
//////////////////////////////////////////////////////////////////////////////////////////////////////// Inverse cipher 
	protected task InvShiftRows;
		byte unsigned tmp_state[1:3][0:3];	// Row 0 of state is not shifted
		
		for (int row=1; row<=3; row++)
        	for (int col=0; col<=3; col++) 
				tmp_state[row][col] = state[row][(col+4-row)%4];

    	for (int row=1; row<=3; row++)
        	for (int col=0; col<=3; col++) 
				state[row][col] = tmp_state[row][col];
	endtask

	protected task InvSubBytes;
    	for (int row=0; row<=3; row++)
        	for (int col=0; col<=3; col++) state[row][col] = inv_sbox(state[row][col]);
	endtask

	protected task InvMixColumns;
		byte unsigned tmp_col[0:3];

		for (int i=0; i<=3; i++)
		begin
			tmp_col[0] = mule(state[0][i]) ^ mulb(state[1][i]) ^ muld(state[2][i]) ^ mul9(state[3][i]);
			tmp_col[1] = mul9(state[0][i]) ^ mule(state[1][i]) ^ mulb(state[2][i]) ^ muld(state[3][i]);
			tmp_col[2] = muld(state[0][i]) ^ mul9(state[1][i]) ^ mule(state[2][i]) ^ mulb(state[3][i]);
			tmp_col[3] = mulb(state[0][i]) ^ muld(state[1][i]) ^ mul9(state[2][i]) ^ mule(state[3][i]);
		
			state[0][i] = tmp_col[0];
			state[1][i] = tmp_col[1];
			state[2][i] = tmp_col[2];
			state[3][i] = tmp_col[3];
		end
	endtask

	protected task SubBytes;
		for (int row=0; row<=3; row++)
			for (int col=0; col<=3; col++) state[row][col] = sbox(state[row][col]);
	endtask

	protected task ShiftRows;
		byte unsigned tmp_state[1:3][0:3];	// Row 0 of state is not shifted
		
		for (int row=1; row<=3; row++)
			for (int col=0; col<=3; col++) tmp_state[row][col] = state[row][(col+row)%4];

		for (int row=1; row<=3; row++)
			for (int col=0; col<=3; col++) state[row][col] = tmp_state[row][col];
	endtask
	
	protected task MixColumns;
		byte unsigned tmp_col[0:3];

		for (int i=0; i<=3; i++)
		begin
			tmp_col[0] = mul2(state[0][i]) ^ mul3(state[1][i]) ^ state[2][i] ^ state[3][i];
			tmp_col[1] = state[0][i] ^ mul2(state[1][i]) ^ mul3(state[2][i]) ^ state[3][i];
			tmp_col[2] = state[0][i] ^ state[1][i] ^ mul2(state[2][i]) ^ mul3(state[3][i]);
			tmp_col[3] = mul3(state[0][i]) ^ state[1][i] ^ state[2][i] ^ mul2(state[3][i]);
		
			state[0][i] = tmp_col[0];
			state[1][i] = tmp_col[1];
			state[2][i] = tmp_col[2];
			state[3][i] = tmp_col[3];
		end
	endtask

	
	protected task AddRoundKey;
		for (int i=0; i<=3; i++)
			for (int j=0; j<=3; j++) state[j][i] ^= key_state[i_round*4+i][j];
	endtask

	task run(int mode);
	// Run cipher / inverse cipher rounds as defined in section 5.1 / 5.3 of FIPS-197 spec.
	// Model functions as cipher / inverse cipher depending on the value of the parameter func.
	//
	// Two run modes are supported
	// mode=0 -> Run from current round to completion
	// mode=1 -> Run 1 round only
	//
	// For encryption both the LoadPt() and key_expansion() must be called first before calling run().
	// For dncryption both the LoadPt() and key_expansion() must be called first before calling run().
	// This is to ensure the cipher / inverse cipher doesn't work on garbage.
	
		// Only continue if model is loaded and there are unfinished round(s)
		if (loaded & ~done)
		begin
			if (func == decrypt) // Model configured as decryptor
			do
			begin
				unique if (i_round == Nr)
				begin
					`ifdef INTERNAL_DEBUG
					$display ("round \t ");
					$display("%2d: istart\t%h",Nr-i_round,state2array);
					$display("%2d: ik_sch\t%h",Nr-i_round,printkey);
					`endif
					
					done = 0;
					AddRoundKey;
					i_round--;
				end
				else if ((i_round <= Nr-1) && (i_round >= 1))
				begin
					`ifdef INTERNAL_DEBUG
					$display("%2d: istart\t%h",Nr-i_round,state2array);
					`endif
					
					InvShiftRows;
					`ifdef INTERNAL_DEBUG
					$display("%2d: is_row\t%h",Nr-i_round,state2array);
					`endif
					
					InvSubBytes;
					`ifdef INTERNAL_DEBUG
					$display("%2d: is_box\t%h",Nr-i_round,state2array);
					$display("%2d: ik_sch\t%h",Nr-i_round,printkey);
					`endif
					
					AddRoundKey;
					`ifdef INTERNAL_DEBUG
					$display("%2d: ik_add\t%h",Nr-i_round,state2array);
					`endif
					
					InvMixColumns;
					i_round--;
				end
				else if (i_round == 0)
				begin
					`ifdef INTERNAL_DEBUG
					$display("%2d: istart\t%h",Nr-i_round,state2array);
					`endif
					
					InvShiftRows;
					`ifdef INTERNAL_DEBUG
					$display("%2d: is_row\t%h",Nr-i_round,state2array);
					`endif
					
					InvSubBytes;
					`ifdef INTERNAL_DEBUG
					$display("%2d: is_box\t%h",Nr-i_round,state2array);
					$display("%2d: ik_sch\t%h",Nr-i_round,printkey);
					`endif
					
					AddRoundKey;
					`ifdef INTERNAL_DEBUG
					$display("%2d: ioutput\t%h",Nr-i_round,state2array);
					`endif
					
					done = 1;	// Last round completed
					loaded = 0;
				end

				if (mode == 1) break;
			end
			while (done == 0);
			
			else
			// Model configured as encryptor
			do
			begin
				unique if (i_round == 0)
				begin
					`ifdef INTERNAL_DEBUG
					$display ("round \t ");
					$display("%2d: input\t%h",i_round,state2array);
					$display("%2d: k_sch\t%h",i_round,printkey);
					`endif
					
					done = 0;
					AddRoundKey;
					i_round++;
				end
				else if ((i_round <= Nr-1) && (i_round >= 1))
				begin
					`ifdef INTERNAL_DEBUG
					$display("%2d: start\t%h",i_round,state2array);
					`endif
					
					SubBytes;
					`ifdef INTERNAL_DEBUG
					$display("%2d: s_box\t%h",i_round,state2array);
					`endif
					
					ShiftRows;
					`ifdef INTERNAL_DEBUG
					$display("%2d: s_row\t%h",i_round,state2array);
					`endif
					
					MixColumns;
					`ifdef INTERNAL_DEBUG
					$display("%2d: m_col\t%h",i_round,state2array);
					`endif
					
					AddRoundKey;
					`ifdef INTERNAL_DEBUG
					$display("%2d: k_sch\t%h",i_round,printkey);
					`endif
					
					i_round++;
				end
				else if (i_round == Nr)
				begin
					`ifdef INTERNAL_DEBUG
					$display("%2d: start\t%h",i_round,state2array);
					`endif
					
					SubBytes;
					`ifdef INTERNAL_DEBUG
					$display("%2d: s_box\t%h",i_round,state2array);
					`endif
					
					ShiftRows;
					`ifdef INTERNAL_DEBUG
					$display("%2d: s_row\t%h",i_round,state2array);
					`endif
					
					AddRoundKey;
					`ifdef INTERNAL_DEBUG
					$display("%2d: k_sch\t%h",i_round,printkey);
					$display("%2d: output\t%h",i_round,state2array);
					`endif
					
					done = 1;	// Last round completed
					loaded = 0;
				end

				if (mode == 1) break;
			end
			while (done == 0);
		
		end
		// Either ciphertext is not loaded or decryption has already completed	
		else $display("#Info : aes_beh_model::run() has nothing to do");
	endtask
endclass	// aes_decrypt_model

// The following types should be used for declaration of aes class objects in your source code.
// e.g. ....
//		aes256_decrypt_t my_aes_decryptor;
//		bit [0:127] pt;
//		....
//		my_aes_decryptor = new;
//		my_aes_decryptor.key_expansion(256'h.......);
//		my_aes_decryptor.LoadCt(128'h.........);
//		my_aes_descryptor.run(0);
//		pt = my_aes_descryptor.state2array();

typedef aes_beh_model #(.Nk(8),.Nr(14),.func(decrypt)) aes256_decrypt_t;
typedef aes_beh_model #(.Nk(6),.Nr(12),.func(decrypt)) aes192_decrypt_t;
typedef aes_beh_model #(.Nk(4),.Nr(10),.func(decrypt)) aes128_decrypt_t;

typedef aes_beh_model #(.Nk(8),.Nr(14),.func(encrypt)) aes256_encrypt_t;
typedef aes_beh_model #(.Nk(6),.Nr(12),.func(encrypt)) aes192_encrypt_t;
typedef aes_beh_model #(.Nk(4),.Nr(10),.func(encrypt)) aes128_encrypt_t;
