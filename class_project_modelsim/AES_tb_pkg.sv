package axi_lite_pkg;



	localparam bit [0:127] input_plain_text = 128'h00112233445566778899aabbccddeeff; 
	localparam bit [0:127] input_key = 128'h000102030405060708090a0b0c0d0e0f;
	localparam bit [0:127] output_cipher_text = 128'h69c4e0d86a7b0430d8cdb78070b4c55a ; 


    localparam ADDR_WIDTH = 32;
	localparam DATA_WIDTH = 32;
	localparam STRB_WIDTH = DATA_WIDTH / 8;

	localparam RESP_OKAY   = 2'b00;
	localparam RESP_EXOKAY = 2'b01;
	localparam RESP_SLVERR = 2'b10;
	localparam RESP_DECERR = 2'b11;

	typedef logic [ADDR_WIDTH - 1 : 0] addr_t;
	typedef logic [DATA_WIDTH - 1 : 0] data_t;
	typedef logic [STRB_WIDTH - 1 : 0] strb_t;
	typedef logic [1 : 0] resp_t;


	// because the signals are not defined 
	typedef struct packed {
		addr_t addr;
		logic valid;
		logic ready;
	} ar_chan_t;

	// Read Data Channel
	typedef struct packed {
		data_t data;
		resp_t resp;
		logic valid;
		logic ready;
	} r_chan_t;














endpackage;