module generate_reset(
input clk,
output reset);

reg [2:0] count_neg_edge_clk = 3'd0;
reg temp_reset = 1'b1;
localparam clk_cycles_to_reset = 3'd7;

assign reset = temp_reset;

always @ (negedge clk)
begin 
	case (count_neg_edge_clk) 
	clk_cycles_to_reset:
		temp_reset <= 1'b0;
	
	default:
	begin 
		count_neg_edge_clk <= count_neg_edge_clk + 3'd1;
		temp_reset <= 1'b1;
	end
	endcase
end

 
endmodule 
