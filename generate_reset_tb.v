module generate_reset_tb();

reg clk;
wire reset; 

localparam PERIOD = 10;

generate_reset DUT (
.clk (clk),
.reset (reset)
);

always 
begin
	#(PERIOD/2);
	clk = ~clk;
end
initial 
begin 
clk = 1'b0;
#(10*PERIOD)
$finish;

end

endmodule 
