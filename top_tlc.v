module top_tlc(
input clk,
output [2:0] north_l,
output [2:0] south_l,
output [2:0] east_l,
output [2:0] west_l);

wire reset; 

generate_reset DUT (
.clk (clk),
.reset (reset)
);

TLC DUT2 (
.clk (clk),
.reset (reset),
.north_l(north_l),
.south_l(south_l),
.east_l(east_l),
.west_l(west_l)
);
endmodule 

