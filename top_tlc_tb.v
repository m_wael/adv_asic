
`timescale 1 ns / 1 ps
module top_tlc_tb ();

// inputs are registers 
reg clk;

// outputs are wires
wire [2:0] north_l;
wire [2:0] south_l;
wire [2:0] east_l;
wire [2:0] west_l;


localparam PERIOD = 10;

// instantiate test 
top_tlc DUT (
.clk (clk),
.north_l(north_l),
.south_l(south_l),
.east_l(east_l),
.west_l(west_l)
);




// generate clk
always 
begin
	#(PERIOD/2);
	clk = ~clk;
end

// start other signals 
initial begin 
 $display ("time \ttreset \tstate \tnorth \tsouth \teast \twest");
 $monitor ("%g \t%b \t%d \t%b \t%b \t%b \t%b",
           $time,DUT.reset,DUT.DUT2.current_state,north_l,south_l,east_l,west_l);
clk = 0;

#(150*PERIOD)
$finish;

end

endmodule 